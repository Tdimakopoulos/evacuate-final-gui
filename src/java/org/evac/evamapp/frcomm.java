/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evac.evamapp;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.client.urlmanager.UrlManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos
 */
@Named(value = "frcomm")
@ViewScoped
public class frcomm implements Serializable {

    private String frsel;
    private Map<String, String> sensorfiles;
    private String smstext;
    private List<resplist> pdata = new ArrayList();

    public String SendFRMessage(String msg, String fr) {
        UrlManager purl = new UrlManager();
        String purlGet = purl.EVAMAPGetURLFORSENDFRMSG(fr);
        CreateOperations pco = new CreateOperations();
        return pco.PostMessageToFrs(msg, purlGet);
    }

    /**
     * Creates a new instance of frcomm
     */
    public frcomm() {

    }

    /**
     * @return the frsel
     */
    public String getFrsel() {
        return frsel;
    }

    /**
     * @param frsel the frsel to set
     */
    public void setFrsel(String frsel) {
        this.frsel = frsel;
    }

    /**
     * @return the sensorfiles
     */
    public Map<String, String> getSensorfiles() {

        try {
            UrlManager purl = new UrlManager();
            String purlGet = purl.EVAMAPGetURLFORFRS("2");
            CreateOperations pco = new CreateOperations();
            String pp = pco.GetFRSEvamapp(purlGet);
            JSONObject jo2 = new JSONObject(pp);
            JSONArray frs = jo2.getJSONArray("first_responders");

            sensorfiles = new HashMap<String, String>();

            for (int i = 0; i < frs.length(); i++) {

                sensorfiles.put(frs.getJSONObject(i).getString("name"), frs.getJSONObject(i).getString("application_uuid"));
            }
            return sensorfiles;
        } catch (IOException ex) {
            Logger.getLogger(frcomm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(frcomm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * @param sensorfiles the sensorfiles to set
     */
    public void setSensorfiles(Map<String, String> sensorfiles) {
        this.sensorfiles = sensorfiles;
    }

    /**
     * @return the smstext
     */
    public String getSmstext() {
        return smstext;
    }

    /**
     * @param smstext the smstext to set
     */
    public void setSmstext(String smstext) {
        this.smstext = smstext;
    }

    public String GetFrsResponse(String fr, String id) throws IOException, JSONException {
        UrlManager purl = new UrlManager();
        String purlGet = purl.EVAMAPGetURLFORRECEIVEFRMSG(fr, id);
        CreateOperations pco = new CreateOperations();
        String pp = pco.GetFRSEvamappResponse(purlGet);
        System.out.println(""+pp);
        JSONObject jo2 = new JSONObject(pp);
        JSONArray frs = jo2.getJSONArray("first_responder_responses");
        String returns = "";
        for (int i = 0; i < frs.length(); i++) {
            returns = returns + "- " + frs.getJSONObject(i).getString("message");
            System.out.println("---" + frs.getJSONObject(i).getString("message"));
//            System.out.println("---"+frs.getJSONObject(i).getString("id"));
//            System.out.println("---"+frs.getJSONObject(i).getString("application_uuid"));
        }
        return returns;
    }

    public void buttonActionph24(ActionEvent actionEvent) {
        System.out.println("MSG / FR " + smstext + " - " + frsel);
        String idsfr = SendFRMessage(this.smstext, this.frsel);
        try {
            String stowrite = frsel + "\n" + smstext + "\n" + idsfr + "\n";
            Files.write(Paths.get("c:\\dataevac\\frmsg.dat"), stowrite.getBytes(), StandardOpenOption.APPEND);

        } catch (IOException e) {
            //exception handling left as an exercise for the reader
        }
    }

    /**
     * @return the pdata
     */
    public List<resplist> getPdata() {

        pdata.clear();

        try (BufferedReader br = new BufferedReader(new FileReader("c:\\dataevac\\frmsg.dat"))) {
            String line;
            int ipos = 0;
            resplist item = new resplist();
            while ((line = br.readLine()) != null) {
                
                if (ipos == 0) {
                item = new resplist();
                    item.setFr(line);
                    ipos = 1;
                } else if (ipos == 1) {
                    item.setMsg(line);
                    ipos = 2;
                } else if (ipos == 2) {
                    String respmsg = "";
                    try {
                        respmsg=this.GetFrsResponse(item.getFr(), line);
                    } catch (JSONException ex) {
                        System.out.println("Error on json : "+ex.getMessage());
                    }
                    item.setResp(respmsg);
                    pdata.add(item);
                    ipos = 0;
                }
                

            }
        } catch (IOException ex) {
//            Logger.getLogger(frcomm.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error : " + ex.getMessage());
        }

        return pdata;
    }

    /**
     * @param pdata the pdata to set
     */
    public void setPdata(List<resplist> pdata) {
        this.pdata = pdata;
    }

}
