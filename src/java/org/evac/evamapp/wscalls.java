/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evac.evamapp;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.client.urlmanager.UrlManager;
import java.io.IOException;
import org.evacuate.mobimesh.mobimeshdata;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class wscalls {
    
    public void GetFrsDetails(String events) throws IOException, JSONException {
        UrlManager purl = new UrlManager();
        String purlGet = purl.EVAMAPGetURLFORFRS(events);
        CreateOperations pco = new CreateOperations();
        String pp = pco.GetFRSEvamapp(purlGet);
        JSONObject jo2 = new JSONObject(pp);
        JSONArray frs = jo2.getJSONArray("first_responders");
        
        for (int i = 0; i < frs.length(); i++) {
            
            System.out.println("---" + frs.getJSONObject(i).getString("name"));
            System.out.println("---" + frs.getJSONObject(i).getString("id"));
            System.out.println("---" + frs.getJSONObject(i).getString("application_uuid"));
        }
    }
    
    public void GetFrsResponse(String fr, String id) throws IOException, JSONException {
        UrlManager purl = new UrlManager();
        String purlGet = purl.EVAMAPGetURLFORRECEIVEFRMSG(fr, id);
        CreateOperations pco = new CreateOperations();
        String pp = pco.GetFRSEvamappResponse(purlGet);
        System.out.println(""+pp);
        JSONObject jo2 = new JSONObject(pp);
        JSONArray frs = jo2.getJSONArray("first_responder_responses");
        
        for (int i = 0; i < frs.length(); i++) {
            
            System.out.println("---" + frs.getJSONObject(i).getString("message"));
//            System.out.println("---"+frs.getJSONObject(i).getString("id"));
//            System.out.println("---"+frs.getJSONObject(i).getString("application_uuid"));
        }
    }
    
    public String SendFRMessage(String msg, String fr) {
        UrlManager purl = new UrlManager();
        String purlGet = purl.EVAMAPGetURLFORSENDFRMSG(fr);
        CreateOperations pco = new CreateOperations();
        return pco.PostMessageToFrs(msg, purlGet);
    }
    
    public static void main(String[] args) throws IOException, JSONException, InterruptedException {
        wscalls pp = new wscalls();
//        pp.GetFrsDetails("1");
        System.out.println("Sending message : Test Message");
//        String idmsg = pp.SendFRMessage("Test Message", "546D8B8F-99EF-45CE-A9E6-F21D608ACEB7");
        
//        while (true) {
//            System.out.println("Checking response : "+idmsg);
            pp.GetFrsResponse("546D8B8F-99EF-45CE-A9E6-F21D608ACEB7", "21");
//            Thread.sleep(15 * 1000);
//        }
    }
}
