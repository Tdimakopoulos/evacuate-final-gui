/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evac.evamapp;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class resplist {
    
    private String fr;
    private String msg;
    private String resp;

    /**
     * @return the fr
     */
    public String getFr() {
        return fr;
    }

    /**
     * @param fr the fr to set
     */
    public void setFr(String fr) {
        this.fr = fr;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the resp
     */
    public String getResp() {
        return resp;
    }

    /**
     * @param resp the resp to set
     */
    public void setResp(String resp) {
        this.resp = resp;
    }
    
    
}
