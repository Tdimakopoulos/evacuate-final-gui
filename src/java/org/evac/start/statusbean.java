/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evac.start;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import org.exus.alarm.publishalarm;
import org.primefaces.json.JSONException;

/**
 *
 * @author Thomas Dimakopoulos
 */
@ManagedBean
@SessionScoped
public class statusbean {
    private String exitstaus;
    
    

    /**
     * Creates a new instance of statusbean
     */
    public statusbean() {
    }

    /**
     * @return the exitstaus
     */
    public String getExitstaus() {
       
        try {
            publishalarm pa = new publishalarm();
            pa.PublishAlarm();
            String dx=pa.GetStatus();
            
            
            return dx;
        } catch (ResponseMapperException ex) {
        return "Communication Error";} catch (IOException ex) {
        return "Communication Error";} catch (JSONException ex) {
        return "Communication Error";}
//        return "Communication Error";
    }

    /**
     * @param exitstaus the exitstaus to set
     */
    public void setExitstaus(String exitstaus) {
        this.exitstaus = exitstaus;
    }
    
}
