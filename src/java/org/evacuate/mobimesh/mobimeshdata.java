/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evacuate.mobimesh;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class mobimeshdata {
    private String accpoint;
    private String status;
    private String countass;
    private String countheard;

    /**
     * @return the accpoint
     */
    public String getAccpoint() {
        return accpoint;
    }

    /**
     * @param accpoint the accpoint to set
     */
    public void setAccpoint(String accpoint) {
        this.accpoint = accpoint;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the countass
     */
    public String getCountass() {
        return countass;
    }

    /**
     * @param countass the countass to set
     */
    public void setCountass(String countass) {
        this.countass = countass;
    }

    /**
     * @return the countheard
     */
    public String getCountheard() {
        return countheard;
    }

    /**
     * @param countheard the countheard to set
     */
    public void setCountheard(String countheard) {
        this.countheard = countheard;
    }
    
    
}
