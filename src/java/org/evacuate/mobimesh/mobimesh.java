/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evacuate.mobimesh;

import eu.evacuate.og.ws.client.post.CreateOperations;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos
 */
@Named(value = "mobimesh")
@RequestScoped
public class mobimesh {

    private List<mobimeshdata> pdata = new ArrayList();

    /**
     * Creates a new instance of mobimesh
     */
    public mobimesh() {

        try {
            CreateOperations pp = new CreateOperations();
            String ss = pp.GetAccessPoints(null);

            JSONObject jo2 = new JSONObject(ss);
            JSONArray offline = jo2.getJSONArray("offline");
            JSONArray online = jo2.getJSONArray("online");

            for (int i = 0; i < online.length(); i++) {
                mobimeshdata item = new mobimeshdata();
                item.setAccpoint(online.get(i).toString());
                ss = pp.GetHeard(online.get(i).toString());

                JSONObject jo21 = new JSONObject(ss);
                item.setCountass(jo21.get("count").toString());
                System.out.println(jo21.get("count"));

                ss = pp.GetAssociated(online.get(i).toString());

                JSONObject jo213 = new JSONObject(ss);
                item.setCountheard(jo213.get("count").toString());
                System.out.println(jo213.get("count"));
                item.setStatus("Online");
                pdata.add(item);
            }
            
            for (int i = 0; i < offline.length(); i++) {
            mobimeshdata item2 = new mobimeshdata();
                item2.setAccpoint(offline.get(i).toString());
                item2.setStatus("Offline");
                item2.setCountass("0");
                item2.setCountheard("0");
            pdata.add(item2);
            }
            
        } catch (IOException ex) {
            Logger.getLogger(mobimesh.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(mobimesh.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the pdata
     */
    public List<mobimeshdata> getPdata() {
        return pdata;
    }

    /**
     * @param pdata the pdata to set
     */
    public void setPdata(List<mobimeshdata> pdata) {
        this.pdata = pdata;
    }

}
