/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evacuate.sensors;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class sensordata {
    
    private String devicetype;
    private String deviceid;
    private String command;
    private String previouscommand;

    /**
     * @return the devicetype
     */
    public String getDevicetype() {
        return devicetype;
    }

    /**
     * @param devicetype the devicetype to set
     */
    public void setDevicetype(String devicetype) {
        this.devicetype = devicetype;
    }

    /**
     * @return the deviceid
     */
    public String getDeviceid() {
        return deviceid;
    }

    /**
     * @param deviceid the deviceid to set
     */
    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @return the previouscommand
     */
    public String getPreviouscommand() {
        return previouscommand;
    }

    /**
     * @param previouscommand the previouscommand to set
     */
    public void setPreviouscommand(String previouscommand) {
        this.previouscommand = previouscommand;
    }
    
    
    
}
