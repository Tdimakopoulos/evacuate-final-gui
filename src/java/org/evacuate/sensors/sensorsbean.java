/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evacuate.sensors;

import com.indra.sofia2.ssap.kp.implementations.rest.SSAPResourceAPI;
import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import com.indra.sofia2.ssap.kp.implementations.rest.resource.SSAPResource;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.core.Response;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Thomas Dimakopoulos
 */
@Named(value = "sensorsbean")
@RequestScoped
public class sensorsbean {

//185.119.248.15:8080/sib/...    

static String szURL = "http://192.168.0.208:8080/sib/services/api_ssap/";
    static SSAPResourceAPI p = null;
    static String sessionkey;
    static SSAPResource pres = new SSAPResource();
private List<sensordata> pdata=new ArrayList();

    /**
     * Creates a new instance of sensorsbean
     */
    public sensorsbean() {
        try {
            if (p == null) {
                p = new SSAPResourceAPI(szURL);
            }
            
            pres.setJoin(true);
            
            pres.setToken("77bcfc0c6ddb4c92bc9f3d463cea27e6");
            pres.setInstanceKP("SocialKPOnly:SocialKPOnlyIns");
            
            Response presponse = p.insert(pres);
            if (presponse.getStatus() == 200) {
                try {
                    //good
                    sessionkey = p.responseAsSsap(presponse).getSessionKey();
                    System.out.print("Session Key : ");
                    System.out.println(sessionkey);
                } catch (ResponseMapperException ex) {
                    Logger.getLogger(sensorsbean.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("error : " + presponse.getStatus());
            }
            
            String szret = "";
            presponse = p.query(sessionkey, "eVACUATE_WP8ActuatorRoutes", "select * from eVACUATE_WP8ActuatorRoutes", "", "SQLLIKE");
            
            String dd = p.responseAsSsap(presponse).getData();//.getData().toString();
            System.out.println("Query Status : " + dd);
            JSONArray inputArray = new JSONArray(dd);
            for (int i = 0; i < inputArray.length(); i++) {
                JSONObject jo = inputArray.getJSONObject(i);
                JSONObject jo2 = jo.getJSONObject("WP8ActuatorRoutes");
                sensordata item=new sensordata();
                item.setDeviceid(jo2.getString("device_id"));
                item.setDevicetype(jo2.getString("device_type"));
                item.setCommand(jo2.getString("command"));
                item.setPreviouscommand(jo2.getString("previous_command"));
                pdata.add(item);
//                szret = szret + jo2.getString("device_type") + " ID : " + jo2.getString("device_id") + "Previous Command " + jo2.getString("previous_command") + "Command " + jo2.getString("command") + "<br/>";
                
            }
        } catch (ResponseMapperException ex) {
            Logger.getLogger(sensorsbean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(sensorsbean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * @return the pdata
     */
    public List<sensordata> getPdata() {
        return pdata;
    }

    /**
     * @param pdata the pdata to set
     */
    public void setPdata(List<sensordata> pdata) {
        this.pdata = pdata;
    }
    
}
