/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evacuate.social;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.event.AjaxBehaviorEvent;
import javax.xml.ws.WebServiceRef;
import net.sf.json.JSONArray;
import org.exus.alarm.publishalarm;
import org.exus.df.SocialChecker;
import org.exus.social.socialcomm;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class socialdash {

    private String option;
    private List<ttwi> twits;

    private PieChartModel livePieModel = new PieChartModel();

    private String alarm;
    private String threadhold;

    private String szthre;

    public PieChartModel getLivePieModel() {
        System.out.println("org.hl7.dashboard.dashboard.getLivePieModel()");
//         livePieModel = new PieChartModel();
        int random1 = (int) (Math.random() * 1000);
        int random2 = (int) (Math.random() * 1000);

        livePieModel.getData().put("Candidate 1", random1);
        livePieModel.getData().put("Candidate 2", random2);

        livePieModel.setTitle("Votes");
        livePieModel.setLegendPosition("ne");

        return livePieModel;
    }

    /**
     * @param livePieModel the livePieModel to set
     */
    public void setLivePieModel(PieChartModel livePieModel) {
        this.livePieModel = livePieModel;
    }

    /**
     * @return the option
     */
    public String getOption() {
        return option;
    }

    /**
     * @param option the option to set
     */
    public void setOption(String option) {
        this.option = option;
    }

    /**
     * @return the twits
     */
    public List<ttwi> getTwits() {
        twits = new ArrayList();

        socialcomm sc = new socialcomm();

        String sYourJsonString = sc.GetSocialText();
        Object[] arrayReceipients = (Object[]) JSONArray.toArray(JSONArray.fromObject(sYourJsonString));
        System.out.println(arrayReceipients[0]);
        System.out.println(arrayReceipients.length);

        for (int i = 0; i < arrayReceipients.length; i++) {
            ttwi pp = new ttwi();
            pp.setDisplayName((String) arrayReceipients[i]);
            pp.setName((String) arrayReceipients[i]);
            twits.add(pp);
        }

        return twits;
    }

    /**
     * @param twits the twits to set
     */
    public void setTwits(List<ttwi> twits) {
        this.twits = twits;
    }

    /**
     * @return the alarm
     */
    public String getAlarm() {
        SocialChecker sc = new SocialChecker();
        String ss = sc.CheckSpecial();
//        sc.CheckThread();

        alarm = ss;

        return alarm;
    }

    public String saveadigitalsign() {

        publishalarm pa = new publishalarm();
        try {
            pa.PublishAlarm();
        } catch (ResponseMapperException ex) {
            Logger.getLogger(socialdash.class.getName()).log(Level.SEVERE, null, ex);
        }
//        for(int i=0;i<200;i++)
        pa.ResetCount();
        pa.close();
//        try {
//            pa.PublishAlarm();
//        } catch (ResponseMapperException ex) {
//            Logger.getLogger(socialdash.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        pa.ResetCount2();
        return "index?faces-redirect=true";
    }

    public String saveadigitalsign2() {

        FileOutputStream fos = null;
        try {
            File fout = new File("c:\\evac-d\\set.soc");
            fos = new FileOutputStream(fout);
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
            bw.write(this.szthre);

            bw.close();
            return "index?faces-redirect=true";
        } catch (FileNotFoundException ex) {
            Logger.getLogger(socialdash.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(socialdash.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(socialdash.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return "";
    }

    /**
     * @return the threadhold
     */
    public String getThreadhold() {

        SocialChecker sc = new SocialChecker();
//        String ss=sc.CheckSpecial();
        String ss = sc.CheckThread();

        threadhold = ss;

        return threadhold;
    }

    /**
     * @param threadhold the threadhold to set
     */
    public void setThreadhold(String threadhold) {
        this.threadhold = threadhold;
    }

    /**
     * @return the szthre
     */
    public String getSzthre() {
        return szthre;
    }

    /**
     * @param szthre the szthre to set
     */
    public void setSzthre(String szthre) {
        this.szthre = szthre;
    }
}
