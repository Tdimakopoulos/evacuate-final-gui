/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.df;

import com.indra.sofia2.ssap.kp.implementations.rest.exception.ResponseMapperException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.exus.alarm.publishalarm;
import org.exus.social.socialcomm;
import org.exus.socialmain.SocialBridge;
import org.exus.utils.utils;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class SocialChecker {

    public String CheckSpecial() {
        String alal = "";
        socialcomm pp = new socialcomm();
        String socialtext = pp.GetSocialText();
        ExusDF pdf = new ExusDF();
        String s1 = pdf.CheckCrow(socialtext);
        if (s1.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Crowding !");
                publishalarm pa = new publishalarm();
                System.out.println("Sending Alarm !");
                pa.PublishAlarm();
                pa.NowInsertCrowding();
                alal = alal + " Crowding ";
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s2 = pdf.CheckExplosion(socialtext);
        if (s2.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Explosion !");
                publishalarm pa = new publishalarm();
                System.out.println("Sending Alarm !");
                pa.PublishAlarm();
                pa.NowInsertExplosion();
                alal = alal + " Explosion ";
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s3 = pdf.CheckFallenPerson(socialtext);
        if (s3.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Fallen Persion !");
                publishalarm pa = new publishalarm();
                System.out.println("Sending Alarm !");
                pa.PublishAlarm();
                pa.NowInsertFallenPerson();
                alal = alal + " Fallen Persion ";
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s4 = pdf.CheckFire(socialtext);
        if (s4.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Fire !");
                publishalarm pa = new publishalarm();
                System.out.println("Sending Alarm !");
                pa.PublishAlarm();
                pa.NowInsertFire();
                alal = alal + " Fire ";
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String s5 = pdf.CheckTransport(socialtext);
        if (s5.equalsIgnoreCase("-1")) {

        } else {
            try {
                System.out.println("Social Alarm Transportation Halted !");
                publishalarm pa = new publishalarm();
                System.out.println("Sending Alarm !");
                pa.PublishAlarm();
                pa.NowInsertTransportation();
                alal = alal + " Transportation Halted ";
            } catch (ResponseMapperException ex) {
                Logger.getLogger(SocialChecker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return alal;
    }

    public String CheckThread() {
        boolean bpub = false;
        int bp = 0;
        try {
            socialcomm pp = new socialcomm();
            String svalue = pp.GetSocialValues();
            utils pu = new utils();
            String i = pu.GetValues(svalue);

            ExusDF pdf = new ExusDF();
            boolean bs = pdf.CalculateTH(i);
            if (bs == true) {
                if (bpub == false) {
                    System.out.println("Social Alarm Detected !");
                    publishalarm pa = new publishalarm();
                    System.out.println("Sending Alarm !");
                    pa.PublishAlarm();
                    pa.NowInsert();
                    bpub = true;
                    bp = Integer.valueOf(i);
                    return "Messages pass Threadhold";
                } else {
                    int ix = Integer.valueOf(i);
                    if (bp == ix) {

                    } else {
                        System.out.println("Social Alarm Detected !");
                        publishalarm pa = new publishalarm();
                        System.out.println("Sending Alarm !");
                        pa.PublishAlarm();
                        pa.NowInsert();
                        bpub = true;
                        bp = Integer.valueOf(i);
                        return "Messages pass Threadhold";
                    }
                }
            }
//                Thread.sleep(20000);
        } catch (ResponseMapperException ex) {
            Logger.getLogger(SocialBridge.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Messages still under Threadhold";
    }
}
