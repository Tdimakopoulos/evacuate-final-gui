/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.cm.tel.com.manager;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import eu.evacuate.og.ws.dto.ResponseDTO;
import eu.evacuate.og.ws.dto.TetraDTO;
import java.io.IOException;
import org.exus.cm.tel.com.active.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.exus.cm.tel.com.jom.CallFO;
import org.exus.cm.tel.com.jom.calls;
import org.primefaces.context.RequestContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class CallsManager implements Serializable {

    private List<CallFO> calls= new ArrayList();
    
    private CallFO selectedFO;

    private String ssmessage;
    
    
    /**
     * Creates a new instance of ActiveCallsManager
     */
    public CallsManager() {
        System.out.println("Initialize objects !!!!");
        calls.clear();
        CallFO pd=new CallFO();
        pd.setFname("Kostas Andoniou");
        pd.setFstatus("online");
        pd.setFtype("FR Chief");
        pd.setId("3786");
        
        calls.add(pd);
        
        CallFO pd1=new CallFO();
        pd1.setFname("Petros Georgiou");
        pd1.setFstatus("online");
        pd1.setFtype("FR");
        pd.setId("3786");
        calls.add(pd1);
        
        CallFO pd2=new CallFO();
        pd2.setFname("MEOC Athens");
        pd2.setFstatus("online");
        pd2.setFtype("MEOC Static");
        pd.setId("3786");
        calls.add(pd2);
        
        CallFO pd3=new CallFO();
        pd3.setFname("MEOC Athens 2");
        pd3.setFstatus("online");
        pd3.setFtype("MEOC Movable");
        pd.setId("3786");
        calls.add(pd3);
        
        CallFO pd4=new CallFO();
        pd4.setFname("EOC Greece");
        pd4.setFstatus("online");
        pd4.setFtype("EOC");
        pd.setId("3786");
        calls.add(pd4);
    }

    /**
     * @return the calls
     */
    public List<CallFO> getCalls() {
        
        calls.clear();
        CallFO pd=new CallFO();
        pd.setFname("Katherine Gray");
        pd.setFstatus("online");
        pd.setFtype("FR Chief");
        pd.setId("3786");
        calls.add(pd);
        
        CallFO pd1=new CallFO();
        pd1.setFname("Piers Powell");
        pd1.setFstatus("online");
        pd1.setFtype("FR");
        pd.setId("3786");
        calls.add(pd1);
        
        CallFO pd2=new CallFO();
        pd2.setFname("Joanne Hemmings");
        pd2.setFstatus("online");
        pd2.setFtype("FR");
        pd.setId("3786");
        calls.add(pd2);
        
        CallFO pd3=new CallFO();
        pd3.setFname("Rose Welch");
        pd3.setFstatus("online");
        pd3.setFtype("FR");
        pd.setId("3786");
        calls.add(pd3);
        
        CallFO pd4=new CallFO();
        pd4.setFname("Una Watson");
        pd4.setFstatus("online");
        pd4.setFtype("FR Chief");
        pd.setId("3786");
        calls.add(pd4);
        return calls;
    }

    /**
     * @param calls the calls to set
     */
    public void setCalls(List<CallFO> calls) {
        this.calls = calls;
    }

    
  
   public void SendMessage(String id,String szMessage)
   {
       CreateOperations pConnector= new CreateOperations();
        ControlDTO pControl=new ControlDTO();
        pControl.setSystems("OG,*");

        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseDTO pReturn=new ResponseDTO();
        TetraDTO pTetra= new TetraDTO();
        pTetra.setProcedure(id);
        pTetra.setMessage(szMessage);
        try {
            pReturn=pConnector.evacTetra(pTetra);
            System.out.println(pReturn.getTaskId());
        } catch (IOException e) {
            e.printStackTrace();
        }
   }
   
    public String sendmsg() {
//        RequestContext context = RequestContext.getCurrentInstance();
//        FacesMessage msg = null;
//        boolean loggedIn = false;
//
//        if (username != null && username.equals("admin") && password != null && password.equals("admin")) {
//            loggedIn = true;
//            msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Welcome", username);
//        } else {
//            loggedIn = false;
//            msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Login Error", "Invalid credentials");
//        }
//
//        FacesContext.getCurrentInstance().addMessage(null, msg);
//        context.addCallbackParam("loggedIn", loggedIn);
        System.out.println("Send message : "+ ssmessage + " To :" +selectedFO.getFname());
        
        SendMessage("89136",ssmessage);
        SendMessage("89137",ssmessage);
        ssmessage="";
        return "tetra?faces-redirect=true";
    }

    /**
     * @return the selectedFO
     */
    public CallFO getSelectedFO() {
        return selectedFO;
    }

    /**
     * @param selectedFO the selectedFO to set
     */
    public void setSelectedFO(CallFO selectedFO) {
        this.selectedFO = selectedFO;
    }

    /**
     * @return the ssmessage
     */
    public String getSsmessage() {
        return ssmessage;
    }

    /**
     * @param ssmessage the ssmessage to set
     */
    public void setSsmessage(String ssmessage) {
        System.out.println("Set Message");
        if (ssmessage==null) {}
        else if (ssmessage.length()<1) {}
        else{
        this.ssmessage = ssmessage;
        }
    }
}
