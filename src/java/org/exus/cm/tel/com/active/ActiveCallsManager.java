/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.exus.cm.tel.com.active;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.exus.cm.tel.com.jom.calls;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class ActiveCallsManager implements Serializable {

    private List<calls> calls= new ArrayList();
    /**
     * Creates a new instance of ActiveCallsManager
     */
    public ActiveCallsManager() {
    }

    /**
     * @return the calls
     */
    public List<calls> getCalls() {
         calls.clear();
        calls calls1= new calls();
        calls1.setCallmembers("Kostas Andoniou <-> Petros Georgiou");
        calls1.setCalltype("Call");
        calls1.setDuration("1 Min");
        calls1.setInitiator("Kostas Andoniou");
//        calls1.setStart("10 Hours Ago");
        calls1.setStatus("Active");
        calls.add(calls1);
        
        
        
        calls calls2= new calls();
        calls2.setCallmembers("MEOC Athens 2 <-> MEOC Athens");
        calls2.setCalltype("Call");
        calls2.setDuration("3 Mins");
        calls2.setInitiator("MEOC Athens");
//        calls2.setStart("8 Hours Ago");
        calls2.setStatus("Active");
        calls.add(calls2);
        return calls;
    }

    /**
     * @param calls the calls to set
     */
    public void setCalls(List<calls> calls) {
        this.calls = calls;
    }
    
}
