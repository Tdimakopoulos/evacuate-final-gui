/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.beans;

import eu.evacuate.og.ws.client.post.CreateOperations;
import eu.evacuate.og.ws.dto.ControlDTO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class MediaFiles {

    private String filelocation;
    private String filename;
    private List<String> filenames;
    private String filetype;
    private String serverurl;
    private String status;
    String dd2;
    private java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> precords = new ArrayList<eu.exus.digitalsign.ws.MediaFiles_Type>();

    public File[] GetAllFiles() {

        File folder = new File("your/path");
        File[] listOfFiles = folder.listFiles();

        return listOfFiles;
//        for (int i = 0; i < listOfFiles.length; i++) {
//            if (listOfFiles[i].isFile()) {
//                System.out.println("File " + listOfFiles[i].getName());
//            } else if (listOfFiles[i].isDirectory()) {
//                System.out.println("Directory " + listOfFiles[i].getName());
//                
//            }
//        }
    }

    /**
     * Creates a new instance of MediaFiles
     */
    public MediaFiles() {
        filenames = new ArrayList();
     
        
        File folder = new File(File.separator +"datafiles"+File.separator );
File[] listOfFiles = folder.listFiles();

    for (int i = 0; i < listOfFiles.length; i++) {
      if (listOfFiles[i].isFile()) {
        //System.out.println("File " + listOfFiles[i].getName());
          filenames.add(listOfFiles[i].getName());
      } else if (listOfFiles[i].isDirectory()) {
        //System.out.println("Directory " + listOfFiles[i].getName());
      }
    }
        LoadRecords();
    }

    private void LoadRecords() {
        precords.clear();
        precords = findAllMediaFiles();
    }

    private static java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> findAllMediaFiles() {
        eu.exus.digitalsign.ws.MediaFiles_Service service = new eu.exus.digitalsign.ws.MediaFiles_Service();
        eu.exus.digitalsign.ws.MediaFiles port = service.getMediaFilesPort();
        return port.findAllMediaFiles();
    }

    private static void removeMediaFiles(eu.exus.digitalsign.ws.MediaFiles_Type mediaFiles) {
        eu.exus.digitalsign.ws.MediaFiles_Service service = new eu.exus.digitalsign.ws.MediaFiles_Service();
        eu.exus.digitalsign.ws.MediaFiles port = service.getMediaFilesPort();
        port.removeMediaFiles(mediaFiles);
    }

    private static void createMediaFiles(eu.exus.digitalsign.ws.MediaFiles_Type mediaFiles) {
        eu.exus.digitalsign.ws.MediaFiles_Service service = new eu.exus.digitalsign.ws.MediaFiles_Service();
        eu.exus.digitalsign.ws.MediaFiles port = service.getMediaFilesPort();
        port.createMediaFiles(mediaFiles);
    }

    private static void editMediaFiles(eu.exus.digitalsign.ws.MediaFiles_Type mediaFiles) {
        eu.exus.digitalsign.ws.MediaFiles_Service service = new eu.exus.digitalsign.ws.MediaFiles_Service();
        eu.exus.digitalsign.ws.MediaFiles port = service.getMediaFilesPort();
        port.editMediaFiles(mediaFiles);
    }

    /**
     * @return the filelocation
     */
    public String getFilelocation() {
        return filelocation;
    }

    /**
     * @param filelocation the filelocation to set
     */
    public void setFilelocation(String filelocation) {
        this.filelocation = filelocation;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the filetype
     */
    public String getFiletype() {
        return filetype;
    }

    /**
     * @param filetype the filetype to set
     */
    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    /**
     * @return the serverurl
     */
    public String getServerurl() {
        return serverurl;
    }

    /**
     * @param serverurl the serverurl to set
     */
    public void setServerurl(String serverurl) {
        this.serverurl = serverurl;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the precords
     */
    public java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> getPrecords() {
        return precords;
    }

    /**
     * @param precords the precords to set
     */
    public void setPrecords(java.util.List<eu.exus.digitalsign.ws.MediaFiles_Type> precords) {
        this.precords = precords;
    }

    /**
     * *******************
     * Media file button functions
     *
     *
     *********************
     */
    public String removedss() {
        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mmss").toString();
        System.out.println("selected : " + dd);
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd)) {
                System.out.println("Located now remove");
                removeMediaFiles(precords.get(i));
            }
        }
        LoadRecords();

        return "mediafiles?faces-redirect=true";
    }

    public String editdss() {
        String dd = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("mmss").toString();
        System.out.println("selected : " + dd);
        dd2 = dd;
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd)) {
                System.out.println("Located now remove");

                filelocation = precords.get(i).getFilelocation();
                filename = precords.get(i).getFilename();
                filetype = precords.get(i).getFiletype();
                serverurl = precords.get(i).getServerurl();
                status = precords.get(i).getStatus();



            }
        }
        return "EditMediaFile?faces-redirect=true";
    }

    
    public String manual() {
        CreateOperations pConnector = new CreateOperations();
        ControlDTO pControl = new ControlDTO();
        pControl.setSystems("OG");
        Long id1 = null;
        Long id2 = null;
        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            System.out.println("eu.evacuate.beans.MediaFiles.semi()"+e.getMessage());
        }

        return "evaccontrol?faces-redirect=true";
    }
    
    public String auto() {
        CreateOperations pConnector = new CreateOperations();
        ControlDTO pControl = new ControlDTO();
        pControl.setSystems("*,OG");
        Long id1 = null;
        Long id2 = null;
        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            System.out.println("eu.evacuate.beans.MediaFiles.semi()"+e.getMessage());
        }

        return "evaccontrol?faces-redirect=true";
    }
    
    public String semi() {
        CreateOperations pConnector = new CreateOperations();
        ControlDTO pControl = new ControlDTO();
        pControl.setSystems("*,OG");
        Long id1 = null;
        Long id2 = null;
        try {
            pConnector.evacControlSystem(pControl);
        } catch (IOException e) {
            System.out.println("eu.evacuate.beans.MediaFiles.semi()"+e.getMessage());
        }

        return "evaccontrol?faces-redirect=true";
    }
    
    public String saveadigitalsignEdit() {


        System.out.println("-->" + dd2);
        for (int i = 0; i < precords.size(); i++) {
            if (precords.get(i).getId().toString().equalsIgnoreCase(dd2)) {
//                System.out.println("Located now edit");
//         precords.get(i).setIp(ip);
//         precords.get(i).setPort(port);
//         precords.get(i).setNickname(nickname);
//         precords.get(i).setLocation(location);
//         
                precords.get(i).setFilename(filename);
                precords.get(i).setFiletype(filetype);
                precords.get(i).setStatus(status);
                editMediaFiles(precords.get(i));
//                
//
            }
        }

        return "mediafiles?faces-redirect=true";
    }

    public String saveadigitalsign() {


        eu.exus.digitalsign.ws.MediaFiles_Type mediaFiles = new eu.exus.digitalsign.ws.MediaFiles_Type();

        mediaFiles.setFilename(this.filename);
        mediaFiles.setFiletype(this.filetype);
        mediaFiles.setStatus(this.status);

        createMediaFiles(mediaFiles);

        LoadRecords();
        return "mediafiles?faces-redirect=true";
    }

    public String saveadigitalsignc() {

//        eu.exus.digitalsign.ws.DigitalSign digitalSign = new eu.exus.digitalsign.ws.DigitalSign();
//
//        digitalSign.setFileplay(new Long(0));
//        digitalSign.setIp(this.ip);
//        digitalSign.setLocation(this.location);
//        digitalSign.setNickname(this.nickname);
//        digitalSign.setPort(this.status);
//        digitalSign.setStatus("ACTIVE");
//        createDigitalSign(digitalSign);
        LoadRecords();
        return "mediafiles?faces-redirect=true";
    }

    /**
     * @return the filenames
     */
    public List<String> getFilenames() {
        return filenames;
    }

    /**
     * @param filenames the filenames to set
     */
    public void setFilenames(List<String> filenames) {
        this.filenames = filenames;
    }
}
