/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.beans;

import eu.exus.digitalsign.ws.DigitalSign;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class ImagesView {

    private List<String> images;
    private String sfilename; 

    public void ImagesView() {
        
        images = new ArrayList<String>();
            images.add("Assembly_-Point.gif");
images.add("Assembly_-Point.jpg");
images.add("Emergency_-evacuation_-route.jpg");
images.add("Evacuation-Assembly-Point.gif");
images.add("Evacuation-Assembly-Point.jpg");
images.add("Evacuation-_Chute-copy.gif");
images.add("Evacuation-_Chute.jpg");
images.add("Exit-Sign_green.jpg");
images.add("Exit-Sign_red.jpg");
images.add("Fire-Hose.gif");
images.add("Fire-Hose.jpg");
images.add("Left-arrow_Green-.gif");
images.add("Left-arrow_Green-.jpg");
images.add("Left-arrow_Red-.gif");
images.add("Left-arrow_Red-.jpg");
images.add("Right_-arrow-_Green-.gif");
images.add("Right_-arrow-_Green-.jpg");
images.add("Right_-arrow-_red-.gif");
images.add("Right_-arrow-_red-.jpg");
images.add("Staircase_green.jpg");
images.add("Staircase_red.jpg");
images.add("Telephone.jpg");
        
    }
 
    public List<String> getImages() {
        images = new ArrayList<String>();
            
            
            images.add("Assembly_-Point.gif");
images.add("Assembly_-Point.jpg");
images.add("Emergency_-evacuation_-route.jpg");
images.add("Evacuation-Assembly-Point.gif");
images.add("Evacuation-Assembly-Point.jpg");
images.add("Evacuation-_Chute-copy.gif");
images.add("Evacuation-_Chute.jpg");
images.add("Exit-Sign_green.jpg");
images.add("Exit-Sign_red.jpg");
images.add("Fire-Hose.gif");
images.add("Fire-Hose.jpg");
images.add("Left-arrow_Green-.gif");
images.add("Left-arrow_Green-.jpg");
images.add("Left-arrow_Red-.gif");
images.add("Left-arrow_Red-.jpg");
images.add("Right_-arrow-_Green-.gif");
images.add("Right_-arrow-_Green-.jpg");
images.add("Right_-arrow-_red-.gif");
images.add("Right_-arrow-_red-.jpg");
images.add("Staircase_green.jpg");
images.add("Staircase_red.jpg");
images.add("Telephone.jpg");
        return images;
    }

    /**
     * @return the sfilename
     */
    public String getSfilename() {
        return sfilename;
    }

    /**
     * @param sfilename the sfilename to set
     */
    public void setSfilename(String sfilename) {
        this.sfilename = sfilename;
        System.out.println("Filename : "+sfilename);
    }
    
}
