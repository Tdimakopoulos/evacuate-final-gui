/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.alerts;

import eu.esponder.journal.*;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class alerts implements Serializable {

    private DualListModel<String> susernames;

    /**
     * Creates a new instance of journalview
     */
    public alerts() {
        List<String> citiesSource = new ArrayList<String>();
        List<String> citiesTarget = new ArrayList<String>();


        citiesSource.add("Katherine Gray");

        citiesSource.add("Piers	Powell");

        citiesSource.add("Joanne Hemmings");

        citiesSource.add("Rose Welch");
        citiesSource.add("Una Watson");


        susernames = new DualListModel<String>(citiesSource, citiesTarget);
    }

    /**
     * @return the susernames
     */
    public DualListModel<String> getSusernames() {
        return susernames;
    }

    /**
     * @param susernames the susernames to set
     */
    public void setSusernames(DualListModel<String> susernames) {
        this.susernames = susernames;
    }
}
