/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.crisis;

import eu.evacuate.alerts.*;
import eu.esponder.journal.*;
import java.io.IOException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.DualListModel;

/**
 *
 * @author Thomas
 */
@ManagedBean
@SessionScoped
public class crisis implements Serializable {

    private List<crisismodel> crisis;
    private crisismodel selectedcrisis;

    /**
     * Creates a new instance of journalview
     */
    public crisis() {
        crisis = new ArrayList<crisismodel>();
        
        crisismodel pitem2= new crisismodel();
        pitem2.setId(1);
        pitem2.setAlarmtype("General Alarm");
        pitem2.setDescr("Stadium - Unidentified plastic bag large near seat number 403");
        pitem2.setName("Bilbao Stadium");
        pitem2.setSituation("Unidentified Package");
        pitem2.setStatus("In Progress");
        pitem2.setVenuetype("Stadium");
        crisis.add(pitem2);
        
        
        
        crisismodel pitem= new crisismodel();
        pitem.setId(2);
        pitem.setAlarmtype("Fire Alarm");
        pitem.setDescr("Metro Bilbao Main Station - Fire alarm in level 2 is active");
        pitem.setName("Metro Bilbao Main Station");
        pitem.setSituation("Explosion");
        pitem.setStatus("In Progress");
        pitem.setVenuetype("Station");
        crisis.add(pitem);
    }

    /**
     * @return the crisis
     */
    public List<crisismodel> getCrisis() {
        return crisis;
    }

    /**
     * @param crisis the crisis to set
     */
    public void setCrisis(List<crisismodel> crisis) {
        this.crisis = crisis;
    }

    /**
     * @return the selectedcrisis
     */
    public crisismodel getSelectedcrisis() {
        return selectedcrisis;
    }

    /**
     * @param selectedcrisis the selectedcrisis to set
     */
    public void setSelectedcrisis(crisismodel selectedcrisis) {
        this.selectedcrisis = selectedcrisis;
    }
}
