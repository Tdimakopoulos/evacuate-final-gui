/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.evacuate.crisis;

/**
 *
 * @author tdim
 */
public class crisismodel {
    
    private int id;
    
    private String name;
    private String descr;
    private String status;
    private String Situation;
    private String alarmtype;
    private String venuetype;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the descr
     */
    public String getDescr() {
        return descr;
    }

    /**
     * @param descr the descr to set
     */
    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the Situation
     */
    public String getSituation() {
        return Situation;
    }

    /**
     * @param Situation the Situation to set
     */
    public void setSituation(String Situation) {
        this.Situation = Situation;
    }

    /**
     * @return the alarmtype
     */
    public String getAlarmtype() {
        return alarmtype;
    }

    /**
     * @param alarmtype the alarmtype to set
     */
    public void setAlarmtype(String alarmtype) {
        this.alarmtype = alarmtype;
    }

    /**
     * @return the venuetype
     */
    public String getVenuetype() {
        return venuetype;
    }

    /**
     * @param venuetype the venuetype to set
     */
    public void setVenuetype(String venuetype) {
        this.venuetype = venuetype;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
    
}
