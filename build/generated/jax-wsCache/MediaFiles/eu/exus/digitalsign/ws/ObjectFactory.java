
package eu.exus.digitalsign.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.exus.digitalsign.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "countMediaFiles");
    private final static QName _CountMediaFilesResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "countMediaFilesResponse");
    private final static QName _CreateMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "createMediaFiles");
    private final static QName _CreateMediaFilesWithValues_QNAME = new QName("http://ws.digitalsign.exus.eu/", "createMediaFilesWithValues");
    private final static QName _EditMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "editMediaFiles");
    private final static QName _FindAllMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findAllMediaFiles");
    private final static QName _FindAllMediaFilesResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findAllMediaFilesResponse");
    private final static QName _FindMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findMediaFiles");
    private final static QName _FindMediaFilesResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findMediaFilesResponse");
    private final static QName _FindRangeMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findRangeMediaFiles");
    private final static QName _FindRangeMediaFilesResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findRangeMediaFilesResponse");
    private final static QName _RemoveMediaFiles_QNAME = new QName("http://ws.digitalsign.exus.eu/", "removeMediaFiles");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.exus.digitalsign.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CountMediaFiles }
     * 
     */
    public CountMediaFiles createCountMediaFiles() {
        return new CountMediaFiles();
    }

    /**
     * Create an instance of {@link CountMediaFilesResponse }
     * 
     */
    public CountMediaFilesResponse createCountMediaFilesResponse() {
        return new CountMediaFilesResponse();
    }

    /**
     * Create an instance of {@link CreateMediaFiles }
     * 
     */
    public CreateMediaFiles createCreateMediaFiles() {
        return new CreateMediaFiles();
    }

    /**
     * Create an instance of {@link CreateMediaFilesWithValues }
     * 
     */
    public CreateMediaFilesWithValues createCreateMediaFilesWithValues() {
        return new CreateMediaFilesWithValues();
    }

    /**
     * Create an instance of {@link EditMediaFiles }
     * 
     */
    public EditMediaFiles createEditMediaFiles() {
        return new EditMediaFiles();
    }

    /**
     * Create an instance of {@link FindAllMediaFiles }
     * 
     */
    public FindAllMediaFiles createFindAllMediaFiles() {
        return new FindAllMediaFiles();
    }

    /**
     * Create an instance of {@link FindAllMediaFilesResponse }
     * 
     */
    public FindAllMediaFilesResponse createFindAllMediaFilesResponse() {
        return new FindAllMediaFilesResponse();
    }

    /**
     * Create an instance of {@link FindMediaFiles }
     * 
     */
    public FindMediaFiles createFindMediaFiles() {
        return new FindMediaFiles();
    }

    /**
     * Create an instance of {@link FindMediaFilesResponse }
     * 
     */
    public FindMediaFilesResponse createFindMediaFilesResponse() {
        return new FindMediaFilesResponse();
    }

    /**
     * Create an instance of {@link FindRangeMediaFiles }
     * 
     */
    public FindRangeMediaFiles createFindRangeMediaFiles() {
        return new FindRangeMediaFiles();
    }

    /**
     * Create an instance of {@link FindRangeMediaFilesResponse }
     * 
     */
    public FindRangeMediaFilesResponse createFindRangeMediaFilesResponse() {
        return new FindRangeMediaFilesResponse();
    }

    /**
     * Create an instance of {@link RemoveMediaFiles }
     * 
     */
    public RemoveMediaFiles createRemoveMediaFiles() {
        return new RemoveMediaFiles();
    }

    /**
     * Create an instance of {@link MediaFiles_Type }
     * 
     */
    public MediaFiles_Type createMediaFiles_Type() {
        return new MediaFiles_Type();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "countMediaFiles")
    public JAXBElement<CountMediaFiles> createCountMediaFiles(CountMediaFiles value) {
        return new JAXBElement<CountMediaFiles>(_CountMediaFiles_QNAME, CountMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountMediaFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "countMediaFilesResponse")
    public JAXBElement<CountMediaFilesResponse> createCountMediaFilesResponse(CountMediaFilesResponse value) {
        return new JAXBElement<CountMediaFilesResponse>(_CountMediaFilesResponse_QNAME, CountMediaFilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "createMediaFiles")
    public JAXBElement<CreateMediaFiles> createCreateMediaFiles(CreateMediaFiles value) {
        return new JAXBElement<CreateMediaFiles>(_CreateMediaFiles_QNAME, CreateMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMediaFilesWithValues }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "createMediaFilesWithValues")
    public JAXBElement<CreateMediaFilesWithValues> createCreateMediaFilesWithValues(CreateMediaFilesWithValues value) {
        return new JAXBElement<CreateMediaFilesWithValues>(_CreateMediaFilesWithValues_QNAME, CreateMediaFilesWithValues.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "editMediaFiles")
    public JAXBElement<EditMediaFiles> createEditMediaFiles(EditMediaFiles value) {
        return new JAXBElement<EditMediaFiles>(_EditMediaFiles_QNAME, EditMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findAllMediaFiles")
    public JAXBElement<FindAllMediaFiles> createFindAllMediaFiles(FindAllMediaFiles value) {
        return new JAXBElement<FindAllMediaFiles>(_FindAllMediaFiles_QNAME, FindAllMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllMediaFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findAllMediaFilesResponse")
    public JAXBElement<FindAllMediaFilesResponse> createFindAllMediaFilesResponse(FindAllMediaFilesResponse value) {
        return new JAXBElement<FindAllMediaFilesResponse>(_FindAllMediaFilesResponse_QNAME, FindAllMediaFilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findMediaFiles")
    public JAXBElement<FindMediaFiles> createFindMediaFiles(FindMediaFiles value) {
        return new JAXBElement<FindMediaFiles>(_FindMediaFiles_QNAME, FindMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindMediaFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findMediaFilesResponse")
    public JAXBElement<FindMediaFilesResponse> createFindMediaFilesResponse(FindMediaFilesResponse value) {
        return new JAXBElement<FindMediaFilesResponse>(_FindMediaFilesResponse_QNAME, FindMediaFilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRangeMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findRangeMediaFiles")
    public JAXBElement<FindRangeMediaFiles> createFindRangeMediaFiles(FindRangeMediaFiles value) {
        return new JAXBElement<FindRangeMediaFiles>(_FindRangeMediaFiles_QNAME, FindRangeMediaFiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindRangeMediaFilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findRangeMediaFilesResponse")
    public JAXBElement<FindRangeMediaFilesResponse> createFindRangeMediaFilesResponse(FindRangeMediaFilesResponse value) {
        return new JAXBElement<FindRangeMediaFilesResponse>(_FindRangeMediaFilesResponse_QNAME, FindRangeMediaFilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveMediaFiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "removeMediaFiles")
    public JAXBElement<RemoveMediaFiles> createRemoveMediaFiles(RemoveMediaFiles value) {
        return new JAXBElement<RemoveMediaFiles>(_RemoveMediaFiles_QNAME, RemoveMediaFiles.class, null, value);
    }

}
