
package eu.exus.digitalsign.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createMediaFiles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createMediaFiles"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="mediaFiles" type="{http://ws.digitalsign.exus.eu/}mediaFiles" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMediaFiles", propOrder = {
    "mediaFiles"
})
public class CreateMediaFiles {

    protected MediaFiles_Type mediaFiles;

    /**
     * Gets the value of the mediaFiles property.
     * 
     * @return
     *     possible object is
     *     {@link MediaFiles_Type }
     *     
     */
    public MediaFiles_Type getMediaFiles() {
        return mediaFiles;
    }

    /**
     * Sets the value of the mediaFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link MediaFiles_Type }
     *     
     */
    public void setMediaFiles(MediaFiles_Type value) {
        this.mediaFiles = value;
    }

}
