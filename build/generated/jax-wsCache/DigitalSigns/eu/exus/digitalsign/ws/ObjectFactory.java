
package eu.exus.digitalsign.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.exus.digitalsign.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CountDigitalSign_QNAME = new QName("http://ws.digitalsign.exus.eu/", "countDigitalSign");
    private final static QName _CountDigitalSignResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "countDigitalSignResponse");
    private final static QName _CreateDigitalSign_QNAME = new QName("http://ws.digitalsign.exus.eu/", "createDigitalSign");
    private final static QName _CreateDigitalSignWithValues_QNAME = new QName("http://ws.digitalsign.exus.eu/", "createDigitalSignWithValues");
    private final static QName _EditDigitalSign_QNAME = new QName("http://ws.digitalsign.exus.eu/", "editDigitalSign");
    private final static QName _FindAllDigitalSigns_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findAllDigitalSigns");
    private final static QName _FindAllDigitalSignsResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findAllDigitalSignsResponse");
    private final static QName _FindDigitalSign_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findDigitalSign");
    private final static QName _FindDigitalSignResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findDigitalSignResponse");
    private final static QName _FindDigitalSignsRange_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findDigitalSignsRange");
    private final static QName _FindDigitalSignsRangeResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "findDigitalSignsRangeResponse");
    private final static QName _RemoveDigitalSign_QNAME = new QName("http://ws.digitalsign.exus.eu/", "removeDigitalSign");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.exus.digitalsign.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CountDigitalSign }
     * 
     */
    public CountDigitalSign createCountDigitalSign() {
        return new CountDigitalSign();
    }

    /**
     * Create an instance of {@link CountDigitalSignResponse }
     * 
     */
    public CountDigitalSignResponse createCountDigitalSignResponse() {
        return new CountDigitalSignResponse();
    }

    /**
     * Create an instance of {@link CreateDigitalSign }
     * 
     */
    public CreateDigitalSign createCreateDigitalSign() {
        return new CreateDigitalSign();
    }

    /**
     * Create an instance of {@link CreateDigitalSignWithValues }
     * 
     */
    public CreateDigitalSignWithValues createCreateDigitalSignWithValues() {
        return new CreateDigitalSignWithValues();
    }

    /**
     * Create an instance of {@link EditDigitalSign }
     * 
     */
    public EditDigitalSign createEditDigitalSign() {
        return new EditDigitalSign();
    }

    /**
     * Create an instance of {@link FindAllDigitalSigns }
     * 
     */
    public FindAllDigitalSigns createFindAllDigitalSigns() {
        return new FindAllDigitalSigns();
    }

    /**
     * Create an instance of {@link FindAllDigitalSignsResponse }
     * 
     */
    public FindAllDigitalSignsResponse createFindAllDigitalSignsResponse() {
        return new FindAllDigitalSignsResponse();
    }

    /**
     * Create an instance of {@link FindDigitalSign }
     * 
     */
    public FindDigitalSign createFindDigitalSign() {
        return new FindDigitalSign();
    }

    /**
     * Create an instance of {@link FindDigitalSignResponse }
     * 
     */
    public FindDigitalSignResponse createFindDigitalSignResponse() {
        return new FindDigitalSignResponse();
    }

    /**
     * Create an instance of {@link FindDigitalSignsRange }
     * 
     */
    public FindDigitalSignsRange createFindDigitalSignsRange() {
        return new FindDigitalSignsRange();
    }

    /**
     * Create an instance of {@link FindDigitalSignsRangeResponse }
     * 
     */
    public FindDigitalSignsRangeResponse createFindDigitalSignsRangeResponse() {
        return new FindDigitalSignsRangeResponse();
    }

    /**
     * Create an instance of {@link RemoveDigitalSign }
     * 
     */
    public RemoveDigitalSign createRemoveDigitalSign() {
        return new RemoveDigitalSign();
    }

    /**
     * Create an instance of {@link DigitalSign }
     * 
     */
    public DigitalSign createDigitalSign() {
        return new DigitalSign();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountDigitalSign }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "countDigitalSign")
    public JAXBElement<CountDigitalSign> createCountDigitalSign(CountDigitalSign value) {
        return new JAXBElement<CountDigitalSign>(_CountDigitalSign_QNAME, CountDigitalSign.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountDigitalSignResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "countDigitalSignResponse")
    public JAXBElement<CountDigitalSignResponse> createCountDigitalSignResponse(CountDigitalSignResponse value) {
        return new JAXBElement<CountDigitalSignResponse>(_CountDigitalSignResponse_QNAME, CountDigitalSignResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDigitalSign }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "createDigitalSign")
    public JAXBElement<CreateDigitalSign> createCreateDigitalSign(CreateDigitalSign value) {
        return new JAXBElement<CreateDigitalSign>(_CreateDigitalSign_QNAME, CreateDigitalSign.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDigitalSignWithValues }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "createDigitalSignWithValues")
    public JAXBElement<CreateDigitalSignWithValues> createCreateDigitalSignWithValues(CreateDigitalSignWithValues value) {
        return new JAXBElement<CreateDigitalSignWithValues>(_CreateDigitalSignWithValues_QNAME, CreateDigitalSignWithValues.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EditDigitalSign }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "editDigitalSign")
    public JAXBElement<EditDigitalSign> createEditDigitalSign(EditDigitalSign value) {
        return new JAXBElement<EditDigitalSign>(_EditDigitalSign_QNAME, EditDigitalSign.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllDigitalSigns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findAllDigitalSigns")
    public JAXBElement<FindAllDigitalSigns> createFindAllDigitalSigns(FindAllDigitalSigns value) {
        return new JAXBElement<FindAllDigitalSigns>(_FindAllDigitalSigns_QNAME, FindAllDigitalSigns.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllDigitalSignsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findAllDigitalSignsResponse")
    public JAXBElement<FindAllDigitalSignsResponse> createFindAllDigitalSignsResponse(FindAllDigitalSignsResponse value) {
        return new JAXBElement<FindAllDigitalSignsResponse>(_FindAllDigitalSignsResponse_QNAME, FindAllDigitalSignsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindDigitalSign }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findDigitalSign")
    public JAXBElement<FindDigitalSign> createFindDigitalSign(FindDigitalSign value) {
        return new JAXBElement<FindDigitalSign>(_FindDigitalSign_QNAME, FindDigitalSign.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindDigitalSignResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findDigitalSignResponse")
    public JAXBElement<FindDigitalSignResponse> createFindDigitalSignResponse(FindDigitalSignResponse value) {
        return new JAXBElement<FindDigitalSignResponse>(_FindDigitalSignResponse_QNAME, FindDigitalSignResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindDigitalSignsRange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findDigitalSignsRange")
    public JAXBElement<FindDigitalSignsRange> createFindDigitalSignsRange(FindDigitalSignsRange value) {
        return new JAXBElement<FindDigitalSignsRange>(_FindDigitalSignsRange_QNAME, FindDigitalSignsRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindDigitalSignsRangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "findDigitalSignsRangeResponse")
    public JAXBElement<FindDigitalSignsRangeResponse> createFindDigitalSignsRangeResponse(FindDigitalSignsRangeResponse value) {
        return new JAXBElement<FindDigitalSignsRangeResponse>(_FindDigitalSignsRangeResponse_QNAME, FindDigitalSignsRangeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveDigitalSign }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "removeDigitalSign")
    public JAXBElement<RemoveDigitalSign> createRemoveDigitalSign(RemoveDigitalSign value) {
        return new JAXBElement<RemoveDigitalSign>(_RemoveDigitalSign_QNAME, RemoveDigitalSign.class, null, value);
    }

}
