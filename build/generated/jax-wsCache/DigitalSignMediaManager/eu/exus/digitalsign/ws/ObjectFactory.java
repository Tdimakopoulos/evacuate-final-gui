
package eu.exus.digitalsign.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.exus.digitalsign.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PlayMedia_QNAME = new QName("http://ws.digitalsign.exus.eu/", "PlayMedia");
    private final static QName _PlayMediaResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "PlayMediaResponse");
    private final static QName _StopMedia_QNAME = new QName("http://ws.digitalsign.exus.eu/", "StopMedia");
    private final static QName _StopMediaResponse_QNAME = new QName("http://ws.digitalsign.exus.eu/", "StopMediaResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.exus.digitalsign.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PlayMedia }
     * 
     */
    public PlayMedia createPlayMedia() {
        return new PlayMedia();
    }

    /**
     * Create an instance of {@link PlayMediaResponse }
     * 
     */
    public PlayMediaResponse createPlayMediaResponse() {
        return new PlayMediaResponse();
    }

    /**
     * Create an instance of {@link StopMedia }
     * 
     */
    public StopMedia createStopMedia() {
        return new StopMedia();
    }

    /**
     * Create an instance of {@link StopMediaResponse }
     * 
     */
    public StopMediaResponse createStopMediaResponse() {
        return new StopMediaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "PlayMedia")
    public JAXBElement<PlayMedia> createPlayMedia(PlayMedia value) {
        return new JAXBElement<PlayMedia>(_PlayMedia_QNAME, PlayMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlayMediaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "PlayMediaResponse")
    public JAXBElement<PlayMediaResponse> createPlayMediaResponse(PlayMediaResponse value) {
        return new JAXBElement<PlayMediaResponse>(_PlayMediaResponse_QNAME, PlayMediaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopMedia }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "StopMedia")
    public JAXBElement<StopMedia> createStopMedia(StopMedia value) {
        return new JAXBElement<StopMedia>(_StopMedia_QNAME, StopMedia.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopMediaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.digitalsign.exus.eu/", name = "StopMediaResponse")
    public JAXBElement<StopMediaResponse> createStopMediaResponse(StopMediaResponse value) {
        return new JAXBElement<StopMediaResponse>(_StopMediaResponse_QNAME, StopMediaResponse.class, null, value);
    }

}
